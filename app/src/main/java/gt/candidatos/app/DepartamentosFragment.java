package gt.candidatos.app;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Chromz on 2/8/2016.
 */
public class DepartamentosFragment extends Fragment {

    ArrayAdapter<String> adaptador;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.dep_fragment, container, false);
        ArrayList<String> departamentos = new ArrayList<>();
        departamentos.add("Alta Verapaz");
        departamentos.add("Baja Verapaz");
        departamentos.add("Chimaltenango");
        departamentos.add("Distrito Central");
        departamentos.add("Distrito Guatemala");
        departamentos.add("El Progreso");
        departamentos.add("Escuintla");
        departamentos.add("Huehuetenango");
        departamentos.add("Izabal");
        departamentos.add("Jalapa");
        departamentos.add("Jutiapa");
        departamentos.add("Petén");
        departamentos.add("Quetzaltenango");
        departamentos.add("Quiché");
        departamentos.add("Retalhuleu");
        departamentos.add("Sacatepéquez");
        departamentos.add("San Marcos");
        departamentos.add("Santa Rosa");
        departamentos.add("Sololá");
        departamentos.add("Totonicapán");
        departamentos.add("Zacapa");
        departamentos.add("Listado Nacional");
        adaptador = new ArrayAdapter<String>(getActivity(),
                R.layout.lista_textview, R.id.lista_txt, departamentos);

        ListView listView = (ListView) rootview.findViewById(R.id.dep_lista);
        listView.setAdapter(adaptador);


        return rootview;
    }


}
