package gt.candidatos.app;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Principal extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ArrayList<JSONObject> candiDatos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*Snackbar.make(findViewById(R.id.drawer_layout),
                "Bienvenido a CandiDatos", Snackbar.LENGTH_LONG).show();*/


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Se inicializa en el fragmento de inicio
        Fragment fragment = new InicioFragment();
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.fragmentoActual, fragment).commit();
        setTitle("Inicio");

        candiDatos = new ArrayList<>();

        ObtenerCandidatos datosProceso = new ObtenerCandidatos();
        datosProceso.execute();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        TextView textView = (TextView)findViewById(R.id.bannerText);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            Fragment currentFragment = getSupportFragmentManager()
                    .findFragmentById(R.id.fragmentoActual);
            if (currentFragment instanceof InicioFragment){
                textView.setText(R.string.bienvenida);
            }else if (currentFragment instanceof RedFragment){
                textView.setText(R.string.contacto);
            }else if (currentFragment instanceof DepartamentosFragment){
                textView.setText(R.string.representante_dialog);
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Fragment fragment = null;
        Class fragmentClass = null;
        TextView textView = (TextView)findViewById(R.id.bannerText);

        if (id == R.id.nav_inicio) {
            fragmentClass = InicioFragment.class;
            setTitle("Inicio");
            textView.setText(R.string.bienvenida);
        } else if (id == R.id.nav_noticias) {
            fragmentClass = InicioFragment.class;
            setTitle("Noticias");
        } else if (id == R.id.nav_representante) {
            fragmentClass = DepartamentosFragment.class;
            setTitle("¿Quién es mi representante?");
            textView.setText(R.string.representante_dialog);
        } else if (id == R.id.nav_comisiones) {
            fragmentClass = RedFragment.class;
            setTitle("Comisiones");
            textView.setText(R.string.comisiones_dialog);
        } else if (id == R.id.nav_contacto) {
            fragmentClass = RedFragment.class;
            setTitle("Contáctanos");
            textView.setText(R.string.contacto);
        } else if (id == R.id.nav_info){
            fragmentClass = RedFragment.class;
            setTitle("Red Ciudadana");
            textView.setText(R.string.acercaDe);
        }

        try{
            fragment = (Fragment) fragmentClass.newInstance();
        }catch(Exception e){
            Log.e("Principal.java", "Instance Error",e);
        }
        FragmentTransaction transaccion = getSupportFragmentManager().beginTransaction();
        transaccion.setCustomAnimations(android.R.anim.slide_in_left,
                android.R.anim.slide_out_right);
        transaccion.replace(R.id.fragmentoActual, fragment)
                .addToBackStack(fragment.getClass().getName())
                .commit();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void clickMenu(View v){
        Button boton = (Button) v;
        int btnID = boton.getId();
        Fragment fragment = null;
        Class fragmentClass = null;
        TextView textView = (TextView)findViewById(R.id.bannerText);

        if (btnID == R.id.btnRepr){
            fragmentClass = DepartamentosFragment.class;
            setTitle("¿Quién es mi representante?");
            textView.setText(R.string.representante_dialog);
        }else if (btnID == R.id.btnComisiones){
            fragmentClass = RedFragment.class;
            setTitle("Comisiones");
            textView.setText(R.string.comisiones_dialog);
        }else if (btnID == R.id.btnTuVoz){
            fragmentClass = RedFragment.class;
            setTitle("Tu voz cuenta");
            textView.setText(R.string.tuvoz);
        }else if (btnID == R.id.btnContacto){
            fragmentClass = RedFragment.class;
            setTitle("Contáctanos");
            textView.setText(R.string.contacto);
        }
        try{
            fragment = (Fragment) fragmentClass.newInstance();
        }catch (Exception e){
            Log.e("Principal.java", "Instance Error",e);
        }
        FragmentTransaction transaccion = getSupportFragmentManager().beginTransaction();
        transaccion.setCustomAnimations(android.R.anim.slide_in_left,
                android.R.anim.slide_out_right);
        transaccion.replace(R.id.fragmentoActual, fragment)
                .addToBackStack(fragment.getClass().getName())
                .commit();
    }

    protected ArrayList<JSONObject> getCandiDatos(){
        return candiDatos;
    }

    protected void setCandidatos(ArrayList<JSONObject> candiDatos){
        this.candiDatos = candiDatos;
    }

    // SOLAMENTE FUNCIONA UNA PAGINA CAMBIAR ESTO !!!! ANTES DE ENTREGAR!!!
    public class ObtenerCandidatos extends AsyncTask<Void, Void,String> {

        // Titulo de la accion
        private final String LOG_TAG = ObtenerCandidatos.class.getSimpleName();

        @Override
        protected String doInBackground(Void... params) {
            // Conexion
            HttpURLConnection urlConnection = null;
            // Buffer
            BufferedReader reader = null;

            String datosJson = null;
            String formato = "json";

            try{

                // Datos de la direccion de la API
                final String URL_BASE = "http://www.candidatos.gt/api/persons/?";
                final String FORMATO = "format";

                // Se crea la direccion de la API
                Uri uri = Uri.parse(URL_BASE)
                        .buildUpon()
                        .appendQueryParameter(FORMATO, formato)
                        .build();
                URL url = new URL(uri.toString());
                // Crear conexion y abrirla
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if(inputStream == null){
                    // No hay datos
                    return null;
                }

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null){
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0 ){
                    return null;
                }

                datosJson = buffer.toString();
                return datosJson;
            }catch (MalformedURLException ex){
                Log.e(LOG_TAG, "Error", ex);
                return null;
            }catch(IOException e){
                Log.e(LOG_TAG, "Error", e);
                return null;
            }finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null){
                    try{
                        reader.close();
                    }catch (final IOException ex){
                        Log.e(LOG_TAG, "Error al cerrar la transferencia", ex);
                    }
                }
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ArrayList<JSONObject> datos = new ArrayList<JSONObject>();
            try {
                JSONObject rootObject = new JSONObject(s);
                JSONArray resultados = rootObject.getJSONArray("result");
                for (int i = 0; i < resultados.length(); i++){
                    datos.add(resultados.getJSONObject(i));
                }
                Principal.this.setCandidatos(datos);
            }catch (JSONException jsonEx){
                Log.e("InicioFragment.java", "JSON PARSE ERROR", jsonEx);
            }

        }
    }
}
